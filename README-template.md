This README documents the use of templates to create the website

In a template file, the code will try to replace any string surrounded by '%%' delimiters: %%template%% will be replaced by the content of the template (of which more to come...)

The index.html files, which contain the dynamic content of the site, are automatically generated from the templates located in the templates/html folder.  
The overall structure of the file is described in template file 'index'
This index may itself refer to 4 predefined templates files (located in the same folder):
*  header: page header
*  banner: page banner
*  content: page content
*  footer: page footer

The content template makes use of a pre-defined template file: 'item-content': this template will be repeated for each content item (each published linked)

Multi-language selection is performed through the pre-defined template 'selector-lang' (if multiple language support is configured)

There is a series of predefined templates, which content is derived from the configuration variables:
*  remote-URL
*  title
*  html-reload-timeout
*  html-banner-image
*  html-banner-text
*  href-[No]: constant link references provided in configuration
*  img-[No]-file: images files

'date' is, the date, and will be filled up using the datetime-format and timezone information from the configuration

There are also pre-defined templates for the published link meta-information:
*  content-url
*  content-title
*  content-image
*  content-author
*  content-timestamp
*  content-description

Last but not least, the user may define any number of new templates by adding the corresponding files into the templates/html folder. The generation code will recursively flesh all the templates found in that folder.

