///paste in APP the google app execution path
var APP = "https://script.google.com/macros/s/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/exec;

function escapeHTML(str) {
    // Note: string cast using String; may throw if `str` is non-serializable, e.g. a Symbol.
    // Most often this is not the case though.
    return String(str)
        .replace(/&/g, "&amp;")
        .replace(/"/g, "&quot;").replace(/'/g, "&#39;")
        .replace(/</g, "&lt;").replace(/>/g, "&gt;");
}

function toString(a) {
    return ((typeof a != 'undefined') && (a != undefined) ? a : '');
}

var form = document.forms.namedItem("newnews");
form.addEventListener('submit', function(ev) {

    var data = new FormData(form);

    //url or title must be valid
    if ((!data.get("url")) && (!data.get("title")) && (!data.get("description")))
        return;

    var url = data.get("url");
    if (!url) {
        data.append("type", "inline");
        var title = data.get("title");
        if (!title) {
            title = data.get("description");
            var titleLen = 80;
            tmp = title.search(/\r|\n|\./);
            if ((0 < tmp) && (tmp < titleLen)) titleLen = tmp;
            title = title.substring(0, titleLen);
            data.set("title", title);
        }
        url = "http://localhost#" + data.get("title").replace(/\s/g, ''); ///need a distinct valid url for it to be taken in account --can be anything-- a bit of a ack


    } else {
        data.append("type", "Article");
    }
    data.set("url", encodeURI(escapeHTML(url)));
    data.set("title", toString(data.get("title")));
    data.set("author", toString(data.get("author")));
    data.append("type", "inline");
    data.set("description", toString(data.get("description")));
    data.append('image', '');
    data.append('audio', '');
    data.append('video', '');
    data.append('lang', '');

    var xhr = new XMLHttpRequest();
    xhr.open('POST', APP, true);

    xhr.onload = function() {
        console.log(this.responseText);
    };
    xhr.send(data);

    ev.preventDefault();
    form.reset();
}, false);
