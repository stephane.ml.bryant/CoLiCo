This project provides scripts and template files to create small collective, and virtually free link concentrators, reusing freely available internet resources: a google account and a (free) web hosting plan with FTP enabled.

Here are the steps
to follow to create a news concentrator:

I. Google spreasheet & application  
The spreadsheet is used to store the links.  
The following steps outline the procedure. They are described in more details in README-gspread.md.  
I.1. Create a google spreadsheet, with 1 sheet named 'responses', and the 1st row columns of that sheet being:  
time, url, title, author, type, description, image, audio, video, lang  
I.2. Get the spreadsheet-id  
I.3. Retrieve the OAuth 2.0 token for the spreadsheet  
I.4. Create an application associated to the spreadsheet using script editor (in Tools), and replacing the default script by the one provided here as gs/script.gs  
The application must be executable by anyone. Retrieve the API key for the application  


II. Configure the web-extension  
The web-extension is used by sources to send the links to be concentrated. For now only mozilla and chrome (& derived browsers) are supported.  
The webextension template is in in webextension/simple.  
II.1 in webextension/simple/linksnap.js, at the top, paste the application API key into the APP variable.  
The extension is ready to be used, but you may want to rename it (search & replace CoLiCo), and set you own icons in icons.   
II.2 pre-test the extension using the developper mode  
II.3 package the extensions for each browser type  

III. Configure the publishing website  
-a basic website template is provided in website  
-in js/newnews.js, at the top, paste the application key into the APP variable.  
-it uses templates define in templates/html, to define the look & feel, and strings defined in strings (to allow for multiple language support)  
-basically the website will be created by performing a recursive template fleshing using %%template-name%% scheme, applied to templates in the templates folder, strings, and a few pre-defined key-words.
the place-holder %%selector-lang%% is used for multi-language support (basically it will be replaced by one template per supported language). templates usage is detailed in README-template.md  
III.1 configure the website & service  
    run: python/CoLiCo-config.py  
    This will ask you for the spreadsheet id and OAUth 2.0 token  
    A sample, commented config file is provided as CoLiCo.cfg
If no remote website is provided, the index.html concentrating the link will be saved in the local website folder  
III.2 run the concentrator: python/CoLiCo-publish.py  

This is a pretty bare HOWTO, and the code itself is pretty bare. Will be fleshed up as time passes.  
