#!/usr/bin/python -W ignore
#-*- coding: utf-8 -*-
from __future__ import print_function

import os.path

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

from httplib2 import Http
from datetime import timedelta,datetime
from pprint import pprint
import sys, getopt, io, os
import configparser

import html.base
import html.content

import ftp.upload

import validators

from gspread.conf import *

import gspread.update

def printHelp():
    "print help"
    print("CoLiCo-publish.py [--help] [-v/--verbose] [-c/--config] config")
    print("to create the config file, use the script CoLiCo-cfg.py")
    return

def printWarning(s):
    "print warning message"
    print("WARNING " + s)
    return

def printError(s):
    "print error message"
    print("ERROR " + s)
    return

def checkExcludedDomains(url, excludedDomains):
    for x in excludedDomains:
        if x.decode('utf-8').lower() in url.decode('utf-8').lower():
            return True
    return False

def extractUniqueRows(values, config):
    rowIndex = len(values)+1 #takes header row into account
    uniqueRows = []
    uniqueURLs = []
    excludedDomains = []
    for x in range(0, int(config['global']['excluded-domain-items'])):
        domainName = config['global']['excluded-domain-' + str(x)]
        excludedDomains.append(domainName)

    for row in values[::-1]:
        delete = checkExcludedDomains(row[INDEX_URL], excludedDomains)
        if not delete:
            if (row[INDEX_URL] not in uniqueURLs):
                if validators.url(row[INDEX_URL], True):
                    if html.content.repareRow(row):
                        gspread.update.updateRow(service, spreadsheetID, rowIndex, row)
                    uniqueRows.append(row)
                else:
                    delete = True
                uniqueURLs.append(row[INDEX_URL])
            else:
                delete = True

            linkDate = datetime.strptime(row[INDEX_TIMESTAMP], "%Y-%m-%dT%H:%M:%SZ");
            if (linkDate < threshold):
                delete = True
                if VERBOSE:
                    print("link " + row[1] + "about to be deleted");
        if delete:
            batchUpdateRequestBody = {
                    "requests": [
                        {
                            "deleteDimension": {
                                "range": {
                                    "dimension": "ROWS",
                                    "endIndex": (rowIndex),
                                    "startIndex": (rowIndex-1),
                                    "sheetId": 0
                                    }
                                }
                            }
                        ],
                    "includeSpreadsheetInResponse": False
                    }
            service.spreadsheets().batchUpdate(spreadsheetId=spreadsheetID,
                                           body=batchUpdateRequestBody).execute()

        rowIndex = rowIndex - 1
    return uniqueRows


timeout = timedelta(microseconds=0)
CONFIG = "./CoLiCo.cfg"

VERBOSE = False

try:
    opts, args = getopt.getopt(sys.argv[1:],"hc:v",["help" , "config=","verbose"])
except getopt.GetoptError:
    printHelp
    sys.exit(2)
for opt, arg in opts:
    if opt in ["h", "--help"]:
        printHelp()
        sys.exit()
    elif opt in ("-c", "--config"):
        CONFIG = arg;
    elif opt in ("-v", "--verbose"):
        VERBOSE = True;

if not (os.path.exists(CONFIG) and os.path.isfile(CONFIG)):
    print("CoLiCo needs a valid config file, exiting")
    sys.exit(2)

config = configparser.ConfigParser()
print("using config file " + CONFIG)
config.read(CONFIG)

title = "CoLiCo-example"
try:
    title = config.get('global','title', fallback = title) or title
except:
    printWarning("error parsing title in " + CONFIG + ", using " + title)
    config['global']['title'] = title

timeout = timedelta(microseconds=0)
try:
    timeout = timeout + timedelta(days=int(config.get('global','timeout-days', fallback = 1) or 1))
    timeout = timeout + timedelta(hours=int(config.get('global','timeout-hours', fallback = 0) or 0))
    timeout = timeout + timedelta(minutes=int(config.get('global','timeout-minuts', fallback = 0) or 0))
except:
    timeout = timedelta(days=1)
    printWarning("error parsing timeout in" + CONFIG + ", using " + str(timeout))
config['global']['timeout-days'] = str(timeout.days)
config['global']['timeout-hours'] = '0'
config['global']['timeout-minuts'] = str(timeout.seconds/60)

HTMLLocalPath = "./html/"
try:
    HTMLLocalPath = config.get('HTML','local-path', fallback = HTMLLocalPath) or HTMLLocalPath 
except:
    printWarning("error parsing HTML local-path in " + CONFIG + ", using " + HTMLLocalPath)
    config['HTML']['local-path'] = HTMLLocalPath

HTMLRemotePath = None
try:
    HTMLRemotePath = config.get('HTML','remote-path', fallback = HTMLRemotePath) or HTMLRemotePath
except:
    printWarning("error parsing HTML remote-path in " + CONFIG + ", using " + HTMLRemotePath)
    config['HTML']['remote-path'] = HTMLRemotePath

HTMLRemoteUserName = None
try:
    HTMLRemoteUserName = config.get('HTML','remote-username', fallback = HTMLRemoteUserName) or HTMLRemoteUserName
except:
    printWarning("error parsing HTML remote-username in " + CONFIG + ", using " + HTMLRemoteUserName)
    config['HTML']['remote-username'] = HTMLRemoteUserName

HTMLRemoteCredentials = None
try:
    HTMLRemoteCredentials = config.get('HTML','remote-credentials', fallback = HTMLRemoteCredentials) or HTMLRemoteCredentials
except:
    printWarning("error parsing HTML remote-credentials in " + CONFIG + ", using " + HTMLRemoteCredentials)
    config['HTML']['remote-path'] = HTMLRemoteCredentials

credentialsPath='credentials.json'
try:
    credentialsPath = config.get('gspread', 'credentials', fallback = credentialsPath) or credentialsPath
except:
    printWarning("error parsing credentials path in " + CONFIG + ", using " + credentialsPath)
    config['gspread']['credentials'] = credentialsPath

clientSecret='client_secret.json'
try:
    clientSecret = config.get('gspread', 'client-secret', fallback = clientSecret) or clientSecret
except:
    printWarning("error parsing client secret path in " + CONFIG + ", using " + clientSecret)
    config['gspread']['client-secret'] = clientSecret

spreadsheetID=''
try:
    spreadsheetID = config.get('gspread', 'spreadsheet-id', fallback = spreadsheetID) or spreadsheetID
except:
    printWarning("error parsing spreadsheet ID in " + CONFIG + ", using " + spreadsheetID)
    config['gspread']['spreadsheet-ID'] = spreadsheetID

if VERBOSE:
    print("title: " + title)
    print("timeout: " + str(timeout))
    print("HTML local path: " + HTMLLocalPath)
    print("credentials path: " + credentialsPath)
    print("client secret: " + clientSecret)
    print("spreadsheet ID: " + spreadsheetID)

# Setup the Sheets API
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

creds = None
if os.path.exists(credentialsPath):
    creds = Credentials.from_authorized_user_file(credentialsPath, SCOPES)

if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file(clientSecret, SCOPES)
        creds = flow.run_local_server(port=0)
    # Save the credentials for the next run
    with open(credentialsPath, 'w') as token:
        token.write(creds.to_json())

service = build('sheets', 'v4', credentials=creds)

# Call the Sheets API
result = service.spreadsheets().values().get(spreadsheetId=spreadsheetID,
                                             range=RANGE_READ).execute()
values = result.get('values', [])

now=datetime.utcnow();
threshold = now - timeout;

if VERBOSE:
    print("link older than {} will be removed".format(threshold.isoformat()))

langs = config['global']['supported-languages'].split(',')
langs = [l.strip()[0:2] for l in langs]

uniqueRows = extractUniqueRows(values, config)

index_html = html.base.init(HTMLLocalPath, config, None, langs)
html.content.fill(index_html, uniqueRows, config, None, langs)
html.base.term(index_html)

indexes_html = [html.base.init(HTMLLocalPath, config, l, langs) for l in langs]

for li in range(len(langs)):
    html.content.fill(indexes_html[li], uniqueRows, config, langs[li], langs)
    html.base.term(indexes_html[li])

#If a remote path is provided, use it.
if HTMLRemotePath is not None:
    type_path = HTMLRemotePath.split("://")
    if type_path[0] == 'ftp':
        try:
            ftp.upload.upload(HTMLLocalPath, type_path[1], HTMLRemoteUserName, HTMLRemoteCredentials)
        except Exception as e:
            printError("HTML upload failed: " + str(e))
            sys.exit(2)
    else:
        printError("Undefined transport" + type_path[0])
        sys.exit(2)

        

