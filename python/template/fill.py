#!/usr/bin/python
#-*- coding: utf-8 -*-

import io
import configparser
import os
import datetime
from babel.dates import format_datetime, get_timezone

from gspread.conf import *

def replaceRepetitive(template, config, section, name, stem=None):
    maxItems = int(config[section][name + '-items'])
    for x in range(0, maxItems):
        itemStr = name + '-' + str(x)
        if stem:
            itemStr = itemStr + '-' + stem
        template = template.replace('%%' + itemStr + '%%', config[section][itemStr])
    return template

def replacePathRepetitive(template, config, section, root, name, stem=None, lang=None):
    maxItems = int(config[section][name + '-items'])
    for x in range(0, maxItems):
        itemStr = name + '-' + str(x)
        if stem:
            itemStr = itemStr + '-' + stem
        filePath = config[section][itemStr]
        if lang:
            if filePath.startswith('./'):
                filePath = filePath.replace("./", "/", 1) #insert lang folder
            filePath_lang = lang + "/" + filePath
            if os.path.isfile(os.path.join(root, filePath_lang)):
                filePath = "/" + filePath_lang
        filePath = filePath.replace("//", "/")
        template = template.replace('%%' + itemStr + '%%', filePath)
    return template

def replaceSelector(template, path, selector, selectionSet):
    selectorString = 'selector-' + selector
    selectorPath = os.path.join(path, selectorString)
    if (os.path.isfile(selectorPath)):
            with open(selectorPath) as selectorFile:
                selectorTemplate = selectorFile.read()
                selectorTemplate = selectorTemplate.decode('utf-8')
                selectorReplacementString = ''
                for selection in selectionSet:
                    selectionReplacement = selectorTemplate.replace('%%' + selector + '%%', selection)
                    selectorReplacementString = selectorReplacementString + selectionReplacement

                template = template.replace('%%' + selectorString + '%%', selectorReplacementString)

    return template

class ReplacementNode:
    def __init__(self, template, replacementList):
        self.template = template
        self.replacementList = replacementList

def recursiveReplace(replacementNode, root, selectorRoot, stringRoot, config, lang, langs):
    #replace lang selector
    #(must come first, as selectors are stored in templates too
    ###########################################################
    if langs:
        replacementNode.template = replaceSelector(replacementNode.template, selectorRoot, 'lang', langs)
    #replace templates -- recursive call
    ###################################
    templateFiles = [f for f in os.listdir(root) if os.path.isfile(os.path.join(root, f)) and 'selector' not in f]
    for f in templateFiles:
        filePath = os.path.join(root, f)
        with open(filePath) as templateFile:
            string = templateFile.read()
            string = string.decode('utf-8')
            string = string.rstrip('\r\n')
            placeHolder = "%%" + f + "%%"
            if placeHolder not in replacementNode.replacementList:
                if placeHolder in replacementNode.template:
                    childReplacementList = replacementNode.replacementList[:]
                    childReplacementList.append(placeHolder)
                    childNode = ReplacementNode(string, childReplacementList)
                    recursiveReplace(childNode, root, selectorRoot, stringRoot, config, lang, langs)
                    replacementNode.template = replacementNode.template.replace(placeHolder, childNode.template)
    #now recurse through template directories too
    #############################################
    templateDirs = [f for f in os.listdir(root) if os.path.isdir(os.path.join(root, f))]
    if langs:
        templateDirs = [f for f in templateDirs if f not in langs]
    for d in templateDirs:
        recursiveReplace(replacementNode, root + "/" + d, selectorRoot, stringRoot, config, lang, langs)
    if lang:
        if os.path.isdir(os.path.join(root, lang)):
            recursiveReplace(replacementNode, root + "/" + lang, selectorRoot, stringRoot, config, lang, langs)
    #now recurse through strings
    ############################
    if stringRoot not in root:
        stringPath = stringRoot + "/" + lang
        recursiveReplace(replacementNode, stringPath, selectorRoot, stringRoot, config, lang, langs)

    #constant place holders
    #######################
    if config['HTML']['remote-URL']:
        replacementNode.template = replacementNode.template.replace('%%remote-URL%%', config['HTML']['remote-URL'])
    replacementNode.template = replacementNode.template.replace("%%title%%", config['global']['title'])
    replacementNode.template = replacementNode.template.replace("%%html-reload-timeout-ms%%", config['HTML']['reload-timeout-ms'])
    replacementNode.template = replacementNode.template.replace("%%html-banner-image%%", config['HTML']['banner-image-file'])
    replacementNode.template = replacementNode.template.replace("%%html-banner-text%%", config['HTML']['banner-text'])
    #date
    now = datetime.datetime.now()
    replacementNode.template = replacementNode.template.replace("%%date%%", format_datetime(now, config['global']['datetime-format'], locale=lang, tzinfo=get_timezone(config['global']['timezone'])))
    #href replacement
    replacementNode.template = replacePathRepetitive(replacementNode.template, config, 'HTML', config['HTML']['local-path'],'href', None, lang)
    #image replacement
    replacementNode.template = replacePathRepetitive(replacementNode.template, config, 'HTML', config['HTML']['local-path'], 'img', 'file', lang)


def fillTemplate(template, config, lang, langs):
    if not lang:
        lang = config['global']['locale']
    rootNode = ReplacementNode(template, [])
    recursiveReplace(rootNode, config['global']['template-path'], config['HTML']['template-path'], config['global']['string-path'], config, lang, langs)
    return rootNode.template

def fill(templateFile, config, lang=None, langs=None):
    template = templateFile.read()
    return fillTemplate(template, config, lang, langs)

def fillRow(template, row, config, lang, langs):
    template = template.decode('utf-8')
    template = template.replace("%%content-url%%", row[INDEX_URL])
    template = template.replace("%%content-title%%", row[INDEX_TITLE])
    template = template.replace("%%content-image%%", row[INDEX_IMAGE])
    template = template.replace("%%content-author%%", row[INDEX_AUTHOR])
    template = template.replace("%%content-timestamp%%", row[INDEX_TIMESTAMP])
    template = template.replace("%%content-description%%", row[INDEX_DESCRIPTION])
    template = template.replace("%%content-summary%%", row[INDEX_DESCRIPTION][0:300])
    return fillTemplate(template, config, lang, langs)

def fillContentBody(templateFile, rows, config, lang, langs):
    template = templateFile.read()
    template = template.decode('utf-8')
    if "%%item-content%%" in template:
        with open(config['HTML']['template-path'] + '/item-content') as itemTemplateFile, open(config['HTML']['template-path'] + '/item-inline-content') as inlineTemplateFile:
            itemTemplate = itemTemplateFile.read()
            itemTemplate = itemTemplate.decode('utf-8')
            inlineTemplate = inlineTemplateFile.read()
            inlineTemplate = inlineTemplate.decode('utf-8')
            contentReplacement = ""
            for r in rows:
                if r[INDEX_TYPE] == "inline":
                    contentItem = fillRow(inlineTemplate, r, config, lang, langs)
                    contentReplacement = contentReplacement + contentItem
                else:
                    contentItem = fillRow(itemTemplate, r, config, lang, langs)
                    contentReplacement = contentReplacement + contentItem
            template = template.replace("%%item-content%%", contentReplacement)
    return fillTemplate(template, config, lang, langs)

