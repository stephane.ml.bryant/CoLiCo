import ftplib
import os

def _upload(localPath, ftp):
    files = os.listdir(localPath)
    cwd = os.getcwd()
    os.chdir(localPath)
    for f in files:
        if os.path.isfile(format(f)):
            fh = open(f, 'rb')
            ftp.storbinary('STOR %s' % f, fh)
            fh.close()
        elif os.path.isdir(format(f)):
            try:
                ftp.mkd(f)
            except:
                #ignore failure, which will happen if the folder already
                #exists
                pass
            ftp.cwd(f)
            _upload(format(f), ftp)
            ftp.cwd('..')
    os.chdir(cwd)
    

def upload(localPath, remotePath, username, password):
    server_path = remotePath.split("/", 1)
    ftp = ftplib.FTP(server_path[0], username, password)
    ftpwd = ftp.pwd()
    if (len(server_path) > 1):
        ftp.cwd(server_path[1])
    _upload(localPath, ftp)
    ftp.cwd(ftpwd)
    ftp.quit()

