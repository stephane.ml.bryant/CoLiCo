#!/usr/bin/python
#-*- coding: utf-8 -*-

from googleapiclient.discovery import build


def updateRow(service, spreadsheetID, index, data):
    values = [data]
    body = {
            'values': values
    }
    range_name = 'responses!A%(row)d:J%(row)d' % {"row": index}
    result = service.spreadsheets().values().update(
            spreadsheetId=spreadsheetID, range=range_name,
            valueInputOption="RAW", body=body).execute()
    return

