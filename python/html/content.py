#!usr/bin/python
#-*- coding: utf-8 -*-

import io
import configparser
from HTMLParser import HTMLParser
from bs4 import BeautifulSoup
from urlparse import urlparse
from gspread.conf import *
from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from guess_language import guessLanguage
from lxml import etree
from StringIO import StringIO

import template.fill

def is_html(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200 
            and content_type is not None 
            and content_type.find('html') > -1)


def validate_html(html):
    _html_parser = etree.HTMLParser(recover = False)
    rV = False
    try:
        rV = etree.parse(StringIO(BeautifulSoup(html, "lxml").get_text()), _html_parser)
    except:
        rV = False
    return rV

def readURL(url):
    try:
        with closing(get(url, stream=True, timeout=30)) as resp:
            if is_html(resp):
                return BeautifulSoup(resp.content, 'html.parser')
            else:
                return None

    except RequestException as e:
        return None
    return None

def repareURL(url, page):
    if 'news.google.com' in url:
        try:
            with closing(get(url, stream=True, timeout=30)) as resp:
                url = resp.url

        except RequestException as e:
            pass
    return url

def prepareURL(row, page):
    ROW[INDEX_URL] = repareURL(row[INDEX_URL], page)
    return

def repareTitle(url, page, title):
    if not title:
        if not page:
            page = readURL(url)
        if page:
            tag = page.find("meta", property="og:title")
            if tag:
                title = tag.get("content", None)
            if not title:
                tag = page.find("meta", {'name='"title"})
                if tag:
                    title = tag.get("content", None)
            if not title:
                tag = page.find("title")
                if tag:
                    title = tag.string
    return title

def prepareTitle(row, page):
    row[INDEX_TITLE] = repareTitle(row[INDEX_URL], page, row[INDEX_TITLE])
    if row[INDEX_TITLE]:
        row[INDEX_TITLE] = BeautifulSoup(row[INDEX_TITLE], "lxml").text
    else:
        row[INDEX_TITLE] = row[INDEX_URL]
    return

def repareDescription(url, page, description):
    if not description:
        if not page:
            page = readURL(url)
        if page:
            tag = page.find("meta", property="og:description")
            if tag:
                description = tag.get("content", None)
            if not description:
                tag = page.find("meta", {'name':"description"})
                if tag:
                    description = tag.get("content", None)

    description.replace("\r\n", "<br>");
    description.replace("\r", "<br>");
    description.replace("\n", "<br>");

    if description:
        if not validate_html(description):
            description=""

    return description

def prepareDescription(row, page):
    row[INDEX_DESCRIPTION] = repareDescription(row[INDEX_URL], page, row[INDEX_DESCRIPTION])
    if row[INDEX_DESCRIPTION]:
        row[INDEX_DESCRIPTION] = BeautifulSoup(row[INDEX_DESCRIPTION], "lxml").text
    else:
        row[INDEX_DESCRIPTION] = ""
    return

def repareAuthor(url, page, author):
    if not author:
        if not page:
            page = readURL(url)
        if page:
            tag = page.find("meta", property="og:site_name")
            if tag:
                author = tag.get("content", None)
            if not author:
                tag = page.find("meta", {'name':"author"})
                if tag:
                    author = tag.get("content", None)
        if not author:
            author = urlparse(url).netloc
    return author

def prepareAuthor(row, page):
    row[INDEX_AUTHOR] = repareAuthor(row[INDEX_URL], page, row[INDEX_AUTHOR])
    if row[INDEX_AUTHOR]:
        row[INDEX_AUTHOR] = BeautifulSoup(row[INDEX_AUTHOR], "lxml").text
        htmlParser = HTMLParser()
        row[INDEX_AUTHOR] = htmlParser.unescape(row[INDEX_AUTHOR])
    return

def repareImage(url, page, image):
    if not image:
        if not page:
            page = readURL(url)
        if page:
            tag = page.find("meta", property="og:image")
            if tag:
                image = tag.get("content", None)
            if not image:
                tag = page.find(lambda tag: tag.name == 'link' and tag.get('rel') == ['icon'])
                if tag:
                    image = tag.get('href', None)
            if not image:
                tag = page.find("link", {'rel':"icon"})
                if tag:
                    image = tag.get("href", None)
    return image


def prepareImage(row, page):
    row[INDEX_IMAGE] = repareImage(row[INDEX_URL], page, row[INDEX_IMAGE])
    if not row[INDEX_IMAGE]:
         row[INDEX_IMAGE]= "./image/no-picture.png"
    return

def repareLang(url, page, lang, sampleText=None):
    if lang == 'undefined' or lang is None:
        lang = None
        if not page:
            page = readURL(url)
        if page:
            tag = page.find("html")
            if tag:
                lang = tag.get('xml:lang', None)
            if not lang:
                lang = tag.get('lang', None)
    if (not lang) and sampleText:
        lang = guessLanguage(sampleText)
        if (lang == 'UNKNOWN'):
            lang = None
    if not lang:
        netloc = urlparse(url).netloc
        if (not lang) and netloc.endswith('.fr'):
            lang = 'fr'
        if (not lang) and netloc.endswith('.es'):
            lang = 'es'
        if (not lang) and netloc.endswith('.com.ar'):
            lang = 'es'
        if (not lang) and netloc.endswith('.co'):
            lang = 'es'

    if lang:
        lang = lang[0:2]
        lang = lang.lower()
    return lang

def prepareLang(row, page):
    row[INDEX_LANG] = repareLang(row[INDEX_URL], page, row[INDEX_LANG], row[INDEX_TITLE] + '-' + row[INDEX_DESCRIPTION])
    return

def repareRow(row):
    while len(row) < INDEX_LEN:
        row.append('')

    repared = False
    page=None

    url = row[INDEX_URL]
    row[INDEX_URL] = repareURL(url, page)
    if (url != row[INDEX_URL]):
        url = row[INDEX_URL]
        repared = True

    if not row[INDEX_TITLE]:
        row[INDEX_TITLE]=repareTitle(url, page, row[INDEX_TITLE])
        if row[INDEX_TITLE]:
            repared = True

    if not row[INDEX_DESCRIPTION]:
        row[INDEX_DESCRIPTION]=repareDescription(url, page, row[INDEX_DESCRIPTION])
        if row[INDEX_DESCRIPTION]:
            repared = True

    if not row[INDEX_AUTHOR]:
        row[INDEX_AUTHOR]=repareAuthor(url, page, row[INDEX_AUTHOR])
        if row[INDEX_AUTHOR]:
            repared = True

    if not row[INDEX_IMAGE]:
        row[INDEX_IMAGE]=repareImage(url, page, row[INDEX_IMAGE])
        if row[INDEX_IMAGE]:
            repared = True

    text=""
    if row[INDEX_TITLE]:
        text=row[INDEX_TITLE]
    if row[INDEX_DESCRIPTION]:
        text = text + '-' + row[INDEX_DESCRIPTION]
    lang = repareLang(url, page, row[INDEX_LANG], text)
    if lang != row[INDEX_LANG]:
        row[INDEX_LANG] = lang
        repared = True

    return repared

def prepareRow(row):
    page=None

    prepareTitle(row, page)
    prepareDescription(row, page)
    prepareAuthor(row, page)
    prepareImage(row, page)
    prepareLang(row, page)

    return row

def fill(f, newsArray, config, lang=None, langs=None):
    header= ""
    try:
        with open(config['HTML']['template-path'] + "/header") as templateFile:
            header = template.fill.fill(templateFile, config, lang, langs)
    except:
        pass

    banner=""
    try:
        with open(config['HTML']['template-path'] + '/banner') as templateFile:
            banner = template.fill.fill(templateFile, config, lang, langs)
    except:
        pass

    footer= ""
    try:
        with open(config['HTML']['template-path'] + '/footer') as templateFile:
            footer = template.fill.fill(templateFile, config, lang, langs)
    except:
        pass

    content = ""
    if (len(newsArray) > 0):
        languageSpecificArray = [prepareRow(r) for r in newsArray if lang is None or lang == r[INDEX_LANG] ]
        if (len(languageSpecificArray) > 0):
            with open(config['HTML']['template-path'] + '/content') as templateFile:
                content = template.fill.fillContentBody(templateFile, languageSpecificArray, config, lang, langs)

    index = ""
    with open(config['HTML']['template-path'] + "/index") as templateFile:
        index = templateFile.read()
        index = index.decode('utf-8')
        index = index.replace('%%header%%', header)
        index = index.replace('%%banner%%', banner)
        index = index.replace('%%content%%', content)
        index = index.replace('%%footer%%', footer)
        index = template.fill.fillTemplate(index, config, lang, langs)

    f.write(index)
    return f
