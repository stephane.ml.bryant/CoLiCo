#!usr/bin/python
#-*- coding: utf-8 -*-

import io
import configparser
import os

import template.fill

def init(path, config, lang=None, langs=None):
    indexPath = "/index.html"
    if lang:
        indexPath = "/" + lang + indexPath
    indexPath = path + indexPath
    folderPath = os.path.dirname(indexPath)
    try:
        os.makedirs(folderPath)
    except:
        pass
    f = io.open(indexPath, "w", encoding="utf-8")
    return f


def term(f):
    f.flush()
    f.close
    return

