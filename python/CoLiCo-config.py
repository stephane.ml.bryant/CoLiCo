#!/usr/bin/python
#-*- coding: utf-8 -*-

import os
import sys
import io
import configparser
import shutil

CONFIG="./CoLiCo.cfg"

if (len(sys.argv) > 1):
    CONFIG=sys.argv[1]

print("Using config file " + CONFIG)

config = configparser.ConfigParser()
inConfig = configparser.ConfigParser()
inConfig.read(CONFIG)

def copyIn(inConf, out, section, name):
    out[section][name] = inConf[section][name]

def readIn(inConf, out, section, name, description='', default=None):
    previous = inConf.get(section, name, fallback='')
    inputLine = section + "." + name
    if description:
        inputLine += " (" + description + ")"
    if previous:
        inputLine += " [" + previous + "]"
    elif default:
        inputLine += "[" + default +"]"
    inputLine += ":"
    item = raw_input(inputLine)
    item = item or previous or default
    if item:
        out[section][name] = item
    return item

def copyAs(src, dst, name=''):
    if (os.path.exists(src) and os.path.isfile(src)):
        try:
            os.makedirs(dst)
        except OSError as exc:
            if os.path.isdir(dst):
                pass
            else:
                return ""
        if not name:
            name = os.path.basename(src)
        extension = os.path.splitext(src)[1]
        shutil.copy2(src, dst + "/" + name + extension)
        return name + extension
    print(src + " not found")
    return ""


def readInRepetitive(inConfig, config, section, name):
    itemMax = 0
    try:
        itemMax = int(inConfig[section][name + '-items'])
        for x in range(0, itemMax):
            itemName = name + "-" + str(x)
            copyIn(inConfig, config, section, itemName)
    except:
        pass

    askForNextItem = True
    while askForNextItem:
        inputLine = "add " + name + " item? (provide value. Empty means no)"
        item = raw_input(inputLine)
        if not item:
            askForNextItem = False
        else:
            itemName = name + "-" + str(itemMax)
            config[section][itemName] = item
            itemMax = itemMax + 1
    
    config[section][name + '-items'] = str(itemMax)
    
    return

def copyInAsRepetitive(inConfig, config, dst, section, name):
    itemMax = 0
    try:
        itemMax = int(inConfig[section][name + '-items'])
        for x in range(0, itemMax):
            itemName = name + "-" + str(x)
            copyIn(inConfig, config, section, itemName)
    except:
        pass

    askForNextItem = True
    while askForNextItem:
        inputLine = "add " + name + " item? (provide value. Empty means no)"
        item = raw_input(inputLine)
        if not item:
            askForNextItem = False
        else:
            itemName = name + "-" + str(itemMax)
            config[section][itemName + '-src'] = item
            config[section][itemName + '-file'] = copyAs(item, dst, itemName)
            itemMax = itemMax + 1

    config[section][name + '-items'] = str(itemMax)

    return


# global section
config['global'] = {}
readIn(inConfig, config, 'global', 'title')
readIn(inConfig, config, 'global','timeout-days')
readIn(inConfig, config, 'global','timeout-hours')
readIn(inConfig, config, 'global','timeout-minuts')
readIn(inConfig, config, 'global', 'locale')
readIn(inConfig, config, 'global', 'timezone')
readIn(inConfig, config, 'global', 'datetime-format')
readIn(inConfig, config, 'global', 'supported-languages', 'comma saparated lang identifier list')
readIn(inConfig, config, 'global','local-path', "local folder path", "./")

config['global']['template-path'] = config['global']['local-path'] + "/templates"
config['global']['string-path'] = config['global']['local-path'] + "/strings"

readInRepetitive(inConfig, config, 'global', 'excluded-domain')


#html section
config['HTML'] = {}
config['HTML']['template-path'] = config['global']['template-path'] + "/html"
readIn(inConfig, config, 'HTML','local-path')
readIn(inConfig, config, 'HTML', 'remote-URL')
item = readIn(inConfig, config, 'HTML', 'reload-timeout-ms', 'website reload timeout in msec', '1800000')
item = readIn(inConfig, config, 'HTML','banner-image-src')
if item:
    HTMLImagePath = config['HTML']['local-path'] + "/img/"
    print("copying banner image " + item + " to " + HTMLImagePath)
    config['HTML']['banner-image-file'] = copyAs(item, HTMLImagePath, 'banner')
item = readIn(inConfig, config, 'HTML', 'banner-text')
item = readIn(inConfig, config, 'HTML','favicon-image-src')
if item:
    HTMLImagePath = config['HTML']['local-path'] + "/img/"
    print("copying favicon image " + item + " to " + HTMLImagePath)
    config['HTML']['favicon-image-file'] = copyAs(item, HTMLImagePath, 'favicon')
item = readIn(inConfig, config, 'HTML', 'no-picture-image-src')
if item:
    HTMLImagePath = config['HTML']['local-path'] + "/img/"
    print("copying no picture image " + item + " to " + HTMLImagePath)
    config['HTML']['no-picture-image-file'] = copyAs(item, HTMLImagePath, 'no-picture')
readInRepetitive(inConfig, config, 'HTML', 'href')
copyInAsRepetitive(inConfig, config, HTMLImagePath, 'HTML', 'img')
readIn(inConfig, config, 'HTML','remote-path', '[ftp/tls/ssh]:path')
readIn(inConfig, config, 'HTML','remote-username')
readIn(inConfig, config, 'HTML', 'remote-credentials', 'credentials or password, depending on the connection type')

#web extension section
config['webextension'] = {}
item = readIn(inConfig, config, 'webextension','mozilla-src', 'path to mozilla extension xpi file')
if item:
    WebExtensionPath = config['HTML']['local-path'] + "/webextension/"
    print("copying mozilla web extension to " + WebExtensionPath)
    config['webextension']['mozilla'] = copyAs(item, WebExtensionPath, 'mozilla')
item = readIn(inConfig, config, 'webextension','mozilla-icon-src', 'path to mozilla extension download icon')
if item:
    HTMLImagePath = config['HTML']['local-path'] + "/img/"
    print("copying mozilla web extension icon to " + HTMLImagePath)
    config['webextension']['mozilla-icon'] = copyAs(item, HTMLImagePath, 'mozilla')

item = readIn(inConfig, config, 'webextension','chrome-src', 'path to chrome extension zip file')
if item:
    WebExtensionPath = config['HTML']['local-path'] + "/webextension/"
    print("copying mozilla web extension to " + WebExtensionPath)
    config['webextension']['chrome'] = copyAs(item, WebExtensionPath, 'chrome')

item = readIn(inConfig, config, 'webextension','chrome-icon-src', 'path to chrome extension download icon')
if item:
    HTMLImagePath = config['HTML']['local-path'] + "/img/"
    print("copying chrome web extension icon to " + HTMLImagePath)
    config['webextension']['chrome-icon'] = copyAs(item, HTMLImagePath, 'chrome')

#spread sheet section
config['gspread'] = {}
readIn(inConfig, config, 'gspread','spreadsheet-ID', 'spreadsheet ID')
readIn(inConfig, config, 'gspread', 'client-secret', 'json file containing the client secret')
readIn(inConfig, config, 'gspread', 'credentials', 'file path to store the credentials (the file will be created if it does not exist')


with open(CONFIG, 'w') as f:
    config.write(f)

