This README documents the creation & use of a google spreadsheet in CoLiCo

The google spreadsheet is used as a public storage for the link being concentrated. As such is is accessed in writing by the CoLiCo webextention (that is by anyone), and in reading & writing by the publisher. 

I. Create a google spreadsheet  
I.1 Go to: 
https://docs.google.com/spreadsheets/u/0/  
And follow the instructions to create an empty spreadsheet  
I.2 In the empty spreadsheet, fill the 1st row columns with the following fields:  
time, url, title, author, type, description, image, audio, video, lang  
I.3 Get the spreasheet ID  
The spreadsheet ID will be later asked by CoLico Configuration script.  
It can be retrieved from the spreadsheet URL as shown below:  
https://docs.google.com/spreadsheets/d/[SPREADSHEET-ID]/edit#gid=0  
I.4 Get the spreadsheet OAuth 2.0 Token  
The spreadsheet Auth token will be later asked by CoLiCo configuration script.  
To retrieve the OAuth 2.0, please consult https://developers.google.com/identity/protocols/OAuth2  
  
II. Create an application associated to the spreadsheet  
II.1 In the spreadsheet menu, go to Tools->script editor  
II.2 replace the default script by the one provided in gs/script.gs  
II.3 In the script editor 'Publish' menu, select 'Publish as web application'  
II.4 Copy the application URL.  
The application URL will be used in the web exctension  
II.5 In 'Execute application as', select 'Me'  
II.6 in 'Who has access to the application', select 'EveryOne, even anonymous users'  
II.7 Click on Update  


