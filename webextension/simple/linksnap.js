///paste in APP the google app execution path
var APP = "https://script.google.com/macros/s/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/exec";

function escapeHTML(str) {
    // Note: string cast using String; may throw if `str` is non-serializable, e.g. a Symbol.
    // Most often this is not the case though.
    return String(str)
        .replace(/&/g, "&amp;")
        .replace(/"/g, "&quot;").replace(/'/g, "&#39;")
        .replace(/</g, "&lt;").replace(/>/g, "&gt;");
}

function toString(a)
{
	return ((typeof a != 'undefined') && (a != undefined) ? a : '');
}

function ancillaryInfo()
{
	return {
                title: undefined,
                author: undefined,
                type: undefined,
                description: undefined,
                image: undefined,
                audio: undefined,
                video: undefined,
		lang: undefined,
                };
}

function postURL(url, ancillary)
{
        var safeURL = escapeHTML(url);
        var data = new FormData();
        var xhr = new XMLHttpRequest();
        xhr.open('POST', APP, true);
        data.append('url', safeURL);
	if (ancillary === undefined)
	{
		ancillary = ancillaryInfo();
	}
	data.append('title', toString(ancillary.title));
	data.append('author', toString(ancillary.author));
	data.append('type', toString(ancillary.type))
	data.append('description', toString(ancillary.description))
	data.append('image', toString(ancillary.image));
	data.append('audio', toString(ancillary.audio));
	data.append('video', toString(ancillary.audio));
	data.append('lang', toString(ancillary.lang));

        xhr.onload = function() {
                console.log(this.responseText);
        };
        xhr.send(data);
}

function post()
{
  browser.tabs.query({active: true, windowId: browser.windows.WINDOW_ID_CURRENT})
  .then(tabs => {
	browser.tabs.executeScript({file: "browser-polyfill.js"});
	browser.tabs.executeScript({
		file: "./content/ancillaryInfo.js"
  		})
		.then(ancillaries=> {
			console.info("SUCCESS");
			postURL(tabs[0].url, ancillaries[0]);
		},
		error => {
			console.info(error);
			postURL(tabs[0].url);
		});
	
  },
  tab => {
    console.info(tab);
  });
}

browser.browserAction.onClicked.addListener(post);

function onContextItemCreated() {
  var bkg = browser.extension.getBackgroundPage();
  if (browser.runtime.lastError) {
    bkg.console.log(`Error: ${browser.runtime.lastError}`);
  } else {
    bkg.console.log("Item created successfully");
  }
}

browser.contextMenus.create({
  id: "CoLiCo",
  title: "CoLiCo",
  contexts: ["link"]
}, onContextItemCreated);

browser.contextMenus.onClicked.addListener(function(info, tab) {
  switch (info.menuItemId) {
    case "CoLiCo":
      var ancillary = {};
      if (info.linkText != undefined)
	ancillary.title = info.linkText;
      if (info.mediaType != undefined)
	ancillary.type = info.mediaType;
      postURL(info.linkUrl, ancillary);
      break;
  }
});


