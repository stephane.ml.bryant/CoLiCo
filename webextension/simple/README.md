# CoLiCo

**This add-on sends a page url & meta tags to a google app for treatment**

## What it does

This extension just includes:

* a content script, "ancillaryInfo.js", which packs up info from page meta and og tags to be sent along with the url, and CoLiCo.js, which adds a button and a menu item on links, to post the selected urls.

## What it shows

* A browser button
* A menu item for links

