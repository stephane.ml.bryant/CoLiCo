
function fillAncillaryFromMetaTags(ancillary)
{
        var metas = document.getElementsByTagName('meta');
        for (var i=0; i<metas.length; i++) {
                if (metas[i].getAttribute("name") == "title") {
                        ancillary.title = metas[i].getAttribute("content");
                }
                if (metas[i].getAttribute("name") == "author") {
                        ancillary.author = metas[i].getAttribute("content");
                }
                if (metas[i].getAttribute("name") == "description") {
                        ancillary.description = metas[i].getAttribute("content");
                }
        }
}

function fillAncillaryFromOGProperties(ancillary)
{
        var metas = document.getElementsByTagName('meta');
        for (var i=0; i<metas.length; i++) {
                if (metas[i].getAttribute("property") == "og:title") {
                        ancillary.title = metas[i].getAttribute("content");
                }
                if (metas[i].getAttribute("property") == "og:site_name") {
                        ancillary.author = metas[i].getAttribute("content");
                }
                if (metas[i].getAttribute("property") == "og:type") {
                        ancillary.type = metas[i].getAttribute("content");
                }
                if (metas[i].getAttribute("property") == "og:description") {
                        ancillary.description = metas[i].getAttribute("content");
                }
                if (metas[i].getAttribute("property") == "og:image") {
                        ancillary.image = metas[i].getAttribute("content");
                }
                if (metas[i].getAttribute("property") == "og:audio") {
                        ancillary.audio = metas[i].getAttribute("content");
                }
                if (metas[i].getAttribute("property") == "og:video") {
                        ancillary.video = metas[i].getAttribute("content");
                }

        }
}

function extractLang()
{
	var lang = undefined;
	var htmlElements = document.getElementsByTagName('html');
	
	if (htmlElements.length > 0) {
		if (htmlElements[0].hasAttribute("lang"))
			lang = htmlElements[0].getAttribute("lang");
		if (htmlElements[0].hasAttribute("xml:lang"))
			lang = htmlElements[0].getAttribute("xml:lang");
	}
	return lang;
}

function extractAncillaryInfo()
{
        var ancillary = {};

        ancillary.title = document.title;
        ancillary.lang = extractLang();

        fillAncillaryFromMetaTags(ancillary);
        fillAncillaryFromOGProperties(ancillary);

        return ancillary;
}

//export {AncillaryInfo, extractAncillaryInfo};
extractAncillaryInfo();

